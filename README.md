# ml-experiments

Some machine learning experiments

## Setup

Install Jupyter:

```bash
pip3 install jupyter
. $HOME/.profile # or re-login in order to update PATH
```

Install ipython3:

```bash
sudo apt install ipython3
```

Install packages:

```bash
pip3 install matplotlib numpy seaborn sklearn scipy
```
